# Exercices sur les bases de données

Ce site propose différents exercices sur les bases de donnée et le langage `SQL`.

Le site est accessible en ligne : https://nreveret.forge.apps.education.fr/exercices_bdd/

![logo](docs/logo.png)
